/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.java.net.zendah;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ParamsFragment extends Fragment {

	public static final String TAG = ParamsFragment.class.getSimpleName();
	private static final String ARG_POSITION = "position";
	public static View rootView = null;
	ImageView moisture;
	TextView temperature, light;
	Button refresh;
	Global global;

	private int position;

	public static ParamsFragment newInstance() {
		ParamsFragment f = new ParamsFragment();

		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	public Bitmap drawTextToBitmap(Bitmap bitmap, String gText) {
		Resources resources = getActivity().getResources();
		float scale = resources.getDisplayMetrics().density;
		// Bitmap bitmap =
		// BitmapFactory.decodeResource(resources, gResId);

		android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();
		// set default bitmap config if none
		if (bitmapConfig == null) {
			bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
		}
		// resource bitmaps are imutable,
		// so we need to convert it to mutable one
		bitmap = bitmap.copy(bitmapConfig, true);

		Canvas canvas = new Canvas(bitmap);
		// new antialised Paint
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		// text color - #3D3D3D
		paint.setColor(Color.parseColor("#FF96AA39"));
		// text size in pixels
		paint.setTextSize((int) (100 * scale));
		// text shadow
		paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

		// draw text to the Canvas center
		Rect bounds = new Rect();
		paint.getTextBounds(gText, 0, gText.length(), bounds);
		int x = (bitmap.getWidth() - bounds.width()) / 2;
		int y = (bitmap.getHeight() + bounds.height()) / 2;

		canvas.drawText(gText, x, y, paint);

		return bitmap;
	}

	private Bitmap drawcircle(Bitmap bmp, int percent) {
		Paint mPaint = new Paint(Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG
				| Paint.ANTI_ALIAS_FLAG);
		mPaint.setDither(true);
		mPaint.setColor(Color.parseColor("#FF96AA39"));
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeWidth(1);

		int size = 450;
		int radius = 360;
		int delta = size - radius;
		int arcSize = (size - (delta / 2)) * 2;
		// int percent = 42;

		Canvas canvas = new Canvas(bmp);

		// Thin circle
		canvas.drawCircle(size, size, radius, mPaint);

		// Arc
		mPaint.setColor(Color.parseColor("#FF96AA39"));
		mPaint.setStrokeWidth(20);
		RectF box = new RectF(delta, delta, arcSize, arcSize);
		float sweep = 360 * percent * 0.01f;
		canvas.drawArc(box, 270, sweep, false, mPaint);

		return bmp;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (rootView != null) {
			ViewGroup parent = (ViewGroup) rootView.getParent();
			if (parent != null) {
				parent.removeView(rootView);
			}
		}
		try {
			rootView = inflater.inflate(R.layout.param, container, false);
		} catch (InflateException e) {

		}

		global = (Global) getActivity().getApplication();

		refresh = (Button) rootView.findViewById(R.id.bt_refresh);
		moisture = (ImageView) rootView.findViewById(R.id.tmp_circle);
		temperature = (TextView) rootView
				.findViewById(R.id.temperature_txt_data);
		light = (TextView) rootView.findViewById(R.id.light_cercle);

		// temperature = (ImageView)
		// rootView.findViewById(R.id.temperat_circle);

		Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
		Bitmap bmp = Bitmap.createBitmap(900, 900, conf);
		Bitmap bmpTemperature = Bitmap.createBitmap(900, 900, conf);
		BitmapDrawable ob;

		if (global.levy != null) {
			Integer x = 100 * Integer.valueOf(global.levy.getMoisture()) / 956; // 956
																				// max
																				// moisture
																				// value
			ob = new BitmapDrawable(drawTextToBitmap(drawcircle(bmp, x), x
					+ " %"));
			temperature.setText(Integer.valueOf(global.levy.getTemperature())
					/ 10 + " �C");
			light.setText(global.levy.getLight() + " Lx");

		} else {
			ob = new BitmapDrawable(drawTextToBitmap(drawcircle(bmp, 0), "0"));

		}

		// BitmapDrawable ob2 = new
		// BitmapDrawable(drawTextToBitmap(drawcircle(bmpTemperature), "22.4"));

		moisture.setImageDrawable(ob);
		// temperature.setImageDrawable(ob2);

		new PortListner(getActivity()).start();

		refresh.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (global.ipAdress != null && global.port != null) {
					String adress = global.ipAdress;
					int port = Integer.parseInt(global.port);

					MyClientTask myClientTask = new MyClientTask(adress, port,
							1);
					myClientTask.execute();
					

				} else {
					Toast.makeText(getActivity().getApplicationContext(),
							"Please set the e-pot box adresse before",
							Toast.LENGTH_LONG).show();
				}

			}
		});

		return rootView;
	}

	public class MyClientTask extends AsyncTask<Void, Void, Void> {

		String dstAddress;
		int dstPort;
		int CMD;
		String response;
		OutputStream outputStream;

		MyClientTask(String addr, int port, int cmd) {
			CMD = cmd;
			dstAddress = addr;
			dstPort = port;
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			try {

				Socket socket = new Socket(dstAddress, dstPort);
				outputStream = socket.getOutputStream();
				try (PrintStream printStream = new PrintStream(outputStream)) {
					printStream.print("" + CMD);
				}

				// InputStream inputStream = socket.getInputStream();
				// ByteArrayOutputStream byteArrayOutputStream = new
				// ByteArrayOutputStream(
				// 1024);
				// byte[] buffer = new byte[1024];
				//
				// int bytesRead;
				// while ((bytesRead = inputStream.read(buffer)) != -1) {
				// byteArrayOutputStream.write(buffer, 0, bytesRead);
				// }

				// socket.close();
				// response = byteArrayOutputStream.toString("UTF-8");

			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//ParamsFragment.newInstance();
//			Fragment prev = getChildFragmentManager().findFragmentByTag("params");
//			if (prev != null) {
//				Log.i("ccccc","dd");
//				DialogFragment df = (DialogFragment) prev;
//				df.dismiss();
//			}
			// Log.i("response ", response);
			// global.setLevies(response);
			super.onPostExecute(result);
		}

	}

}