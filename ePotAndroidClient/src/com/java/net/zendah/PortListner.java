package com.java.net.zendah;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import android.app.Application;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class PortListner extends Thread {

	private ServerSocket serverSocket;
	private Socket clientSocket;
	private PrintWriter out;
	private BufferedReader in;
	FragmentActivity context;

	public PortListner(FragmentActivity fragmentActivity) {

		this.context = fragmentActivity;
	}

	public void run() {
		while (true) {
			try {
				// Open a server socket listening on port 8080
				// InetAddress addr =
				// InetAddress.getByName(getLocalIpAddress());
				serverSocket = new ServerSocket(9191);
				clientSocket = serverSocket.accept();

				InputStream inputStream = clientSocket.getInputStream();
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
						1024);
				byte[] buffer = new byte[1024];

				int bytesRead;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					byteArrayOutputStream.write(buffer, 0, bytesRead);
				}
				String response = byteArrayOutputStream.toString("UTF-8");
				Log.i("response ", response);
				Global global = (Global) context.getApplication();
				global.setLevies(response);
				serverSocket.close();
			} catch (Exception e) {
				// Omitting exception handling for clarity
			}
		}
	}

}