/*
 I build this class to have the right time, because when i disconnect the galileo board,
there is no batery to save time, so it indisponsable to have the right date in DB
 */
package esprit.zendah.pfe;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.NtpV3Packet;
import org.apache.commons.net.ntp.TimeInfo;

/**
 *
 * @author Khalil
 */
public class GetRightTimeFromNtpServer {

    public GetRightTimeFromNtpServer() throws UnknownHostException, IOException, ParseException {
        String TIME_SERVER = "de.pool.ntp.org"; //europeen time server
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
        TimeInfo timeInfo = timeClient.getTime(inetAddress);
        NtpV3Packet message = timeInfo.getMessage();
        long serverTime = message.getTransmitTimeStamp().getTime();
        Date time = new Date(serverTime);
        System.out.println("Time from " + TIME_SERVER + ": " + time);//Thu May 22 21:57:17 UTC 2014
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        String dateStr = dateFormat.format(time.getTime());
        System.out.println("haw il fourma " + dateStr);
        Date d = dateFormat.parse(dateStr);
        Calendar gc = new GregorianCalendar();
        gc.setTime(d);
        gc.add(Calendar.HOUR, 1);
        Date d2 = gc.getTime();
        String d3 = dateFormat.format(d2.getTime());
        System.out.println("finale " + d3);
        Runtime.getRuntime().exec(new String[]{"date", "--set", d3});
    }

}
