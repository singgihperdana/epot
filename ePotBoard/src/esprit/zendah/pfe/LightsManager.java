/*
 ALl light manipulation are centralized in this class to avoid code redendency
 */
package esprit.zendah.pfe;

import esprit.zendah.pin.GreenPinDAO;
import esprit.zendah.pin.RedPinDAO;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Khalil
 */
public class LightsManager {

    RedPinDAO red_pin;
    GreenPinDAO green_pin;
    private static LightsManager INSTANCE;

    public static boolean lightingMode = false;

    public static LightsManager getInstance() {
        //lightingMode = c;
        if (INSTANCE == null) {
            INSTANCE = new LightsManager();
        }
        return INSTANCE;
    }

    LightsManager() {
        red_pin = new RedPinDAO();
        green_pin = new GreenPinDAO();

        green_pin.initReading();
        red_pin.initReading();
    }

//    @Override
//    public void run() {
//        red_pin = new RedPinDAO();
//        green_pin = new GreenPinDAO();
//
//        green_pin.initReading();
//        red_pin.initReading();
//
//        while (true) {
//            System.out.println("I am lightingMode = " + lightingMode);
//            switch (lightingMode) {
//                case 1: {
//                    greenCube();
//
//                    break;
//                }
//                case 2: {
//                    redCube();
//                    break;
//                }
//
//                case 3: {
//                    quickGreenFlashing();
//                    break;
//                }
//                case 4: {
//                    quickRedFlashing();
//                    break;
//                }
//
//                default: {
//                    offAll();
//                    break;
//                }
//
//            }
//
////            try {
////                Thread.currentThread().sleep(1000);
////            } catch (InterruptedException ex) {
////                Logger.getLogger(LightsManager.class.getName()).log(Level.SEVERE, null, ex);
////            }
//        }
//
//    }
//    public static void setCNT(int CNT) {
//        this.CNT = CNT;
//    }
    void offAll() {
        getInstance().lightingMode = false;
        green_pin.ledOff();
        red_pin.ledOff();
    }

    void greenCube() {
        getInstance().lightingMode = false;
        red_pin.ledOff();
        green_pin.ledOn();
    }

    void redCube() {
        getInstance().lightingMode = false;
        green_pin.ledOff();
        red_pin.ledOn();
    }

    void quickGreenFlashing() {
        getInstance().lightingMode = false;
        Thread th = new Thread() {
            public void run() {
                getInstance().lightingMode = true;
                //while (getInstance().lightingMode) {
                while (true) {

                    try {
                        red_pin.ledOff();
                        green_pin.ledOn();
                        Thread.sleep(250);
                        offAll();
                        Thread.sleep(250);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LightsManager.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        };
        th.start();

    }

    void quickRedFlashing() {
        //getInstance().lightingMode = false;
        Thread th = new Thread() {
            public void run() {
                // getInstance().lightingMode = true;
                // while (getInstance().lightingMode) {
                while (true) {
                    try {
                        green_pin.ledOff();
                        red_pin.ledOn();
                        Thread.sleep(250);
                        offAll();
                        Thread.sleep(250);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LightsManager.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        };
        th.start();

    }

    void faddingGreenFlashing() {
    }

    void faddingRedFlashing() {
    }

}
