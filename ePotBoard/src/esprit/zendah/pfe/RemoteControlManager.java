/*
 Here is the listen for socket entrance from android cleint, on port 9090

 */
package esprit.zendah.pfe;

import esprit.zendah.pin.VannePinDAO1;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Khalil
 */
public class RemoteControlManager extends Thread {

    @Override
    public void run() {

        int count = 0;

        //hard code to use port 8080
        try (ServerSocket serverSocket = new ServerSocket(9090)) {

            System.out.println("I'm waiting here: " + serverSocket.getLocalPort());

            while (true) {

                try {
                    Socket socket = serverSocket.accept();

                    count++;
                    System.out.println("#" + count + " from "
                            + socket.getInetAddress() + ":"
                            + socket.getPort());

                    InputStream inputStream = socket.getInputStream();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
                    byte[] buffer = new byte[1024];

                    int bytesRead;
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        byteArrayOutputStream.write(buffer, 0, bytesRead);
                    }

                    // socket.close();
                    String response = byteArrayOutputStream.toString("UTF-8");
                    System.out.println("#from android" + response);
                    int c = Integer.valueOf(response);
                    switch (c) {
                        case 1: {
                            //asking for last levy value, using json

                            HostThread myHostThread = new HostThread(socket, count);
                            myHostThread.start();
                            break;
                        }
                        case 2: {

                            //Asking for water tap opening during 1 sec
                            
                            VannePinDAO1 vanne;
                            vanne = new VannePinDAO1();
                            vanne.initReading();
                            vanne.vanneOn();
                            LightsManager lm = LightsManager.getInstance();
                            lm.redCube();
                            try {
                                Thread.currentThread().sleep(1000);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(RemoteControlManager.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            lm.greenCube();

                            break;
                        }
                        case 3: {

                            LightsManager lm = LightsManager.getInstance();
                            lm.quickGreenFlashing();

                            break;
                        }
                        case 4: {

                            LightsManager lm = LightsManager.getInstance();
                            lm.quickRedFlashing();

                            break;
                        }

                        default: {
                            System.out.println("invalid arg");
                            break;
                        }

                    }

                    /*  move to background thread
                     OutputStream outputStream = socket.getOutputStream();
                     try (PrintStream printStream = new PrintStream(outputStream)) {
                     printStream.print("Hello from Raspberry Pi, you are #" + count);
                     }
                     */
                } catch (IOException ex) {
                    System.out.println(ex.toString());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
//
//        int count = 0;
//
//        //hard code to use port 8080
//        try (ServerSocket serverSocket = new ServerSocket(9090)) {
//
//            System.out.println("I'm waiting here: " + serverSocket.getLocalPort());
//
//            while (true) {
//
//                try {
//                    Socket socket = serverSocket.accept();
//
//                    count++;
//                    System.out.println("#" + count + " from "
//                            + socket.getInetAddress() + ":"
//                            + socket.getPort());
//
//                    InputStream inputStream = socket.getInputStream();
//                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
//                    byte[] buffer = new byte[1024];
//
//                    int bytesRead;
//                    while ((bytesRead = inputStream.read(buffer)) != -1) {
//                        byteArrayOutputStream.write(buffer, 0, bytesRead);
//                    }
//
//                    // socket.close();
//                    String response = byteArrayOutputStream.toString("UTF-8");
//                    System.out.println("#from android" + response);
//
//                    int c = Integer.valueOf(response);
//                    switch (c) {
//                        case 1: {
//
//                            LightsManager lm = LightsManager.getInstance();
//                            lm.greenCube();
//                            break;
//                        }
//                        case 2: {
//
//                            LightsManager lm = LightsManager.getInstance();
//                            lm.redCube();
//
//                            break;
//                        }
//                        case 3: {
//
//                            LightsManager lm = LightsManager.getInstance();
//                            lm.quickGreenFlashing();
//
//                            break;
//                        }
//                        case 4: {
//
//                            LightsManager lm = LightsManager.getInstance();
//                            lm.quickRedFlashing();
//
//                            break;
//                        }
//
//                        default: {
//                            System.out.println("invalid arg");
//                            break;
//                        }
//
//                    }
//                    //doTask(Integer.valueOf(response));
//
//                    /*  move to background thread
//                     OutputStream outputStream = socket.getOutputStream();
//                     try (PrintStream printStream = new PrintStream(outputStream)) {
//                     printStream.print("Hello from Raspberry Pi, you are #" + count);
//                     }
//                     */
////                    HostThread myHostThread = new HostThread(socket, count);
////                    myHostThread.start();
//                } catch (IOException ex) {
//                    System.out.println(ex.toString());
//                }
//            }
//        } catch (IOException ex) {
//            System.out.println(ex.toString());
//        }

    }

//    private static class HostThread extends Thread {
//
//        private Socket hostThreadSocket;
//        int cnt;
//
//        HostThread(Socket socket, int c) {
//            hostThreadSocket = socket;
//            cnt = c;
//        }
//
//        @Override
//        public void run() {
//
//            OutputStream outputStream;
//            try {
//                outputStream = hostThreadSocket.getOutputStream();
//
//                try (PrintStream printStream = new PrintStream(outputStream)) {
//                    printStream.print("Hello from Galileo Pi in background thread, you are #" + cnt);
//                }
//            } catch (IOException ex) {
//                Logger.getLogger(RemoteControlManager.class.getName()).log(Level.SEVERE, null, ex);
//            } finally {
//                try {
//                    hostThreadSocket.close();
//                } catch (IOException ex) {
//                    Logger.getLogger(RemoteControlManager.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//    }
//    private void doTask(int c) {
//
//        switch (c) {
//            case 1: {
//                new OperationsManager().getInstance(1);
//                break;
//            }
//            case 2: {
//                new OperationsManager().getInstance(2);
//                break;
//            }
//
//            default: {
//                System.out.println("invalid arg");
//                break;
//            }
//
//        }
//    }
}
