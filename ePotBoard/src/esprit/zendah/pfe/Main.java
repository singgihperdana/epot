/*
 The main class of our project, i create 4 threads, each make a task, it help me to have a clear code
    I use the sqlite jdbc as library to connect to db, and java json to generate json
 */
package esprit.zendah.pfe;

import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 *
 * @author Khalil
 */
public class Main {

    static int leviesPeriode = 60000;//levies every 1 minute


    public static void main(String[] args) throws SQLException, IOException, UnknownHostException, ParseException {

        new GetRightTimeFromNtpServer();

        LightsManager lm = new LightsManager().getInstance();
        lm.greenCube();
        

        Thread levies = new LeviesManager(leviesPeriode);
        levies.start();

        Thread op = new OperationsManager();
        op.start();

        RemoteControlManager rm = new RemoteControlManager();
        rm.start();

        //vanne = new VannePinDAO1();
//        ms = new MoisturePinDAO();
//        tp = new TemperaturePinDAO1();
//        ls = new LightSensorPinDAO();
////        red_pin = new RedPinDAO();
////        green_pin = new GreenPinDAO();
//
//        ms.initReading();
//       // vanne.initReading();
////        green_pin.initReading();
////        red_pin.initReading();
//        ls.initReading();
//        tp.initReading();
//
//        Calendar cal;
//        DateFormat dateFormat
//                = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
//        String dateStr;
//        int sleepPeriod = 500;
        // Loop forever
        //while (true) {

//            // Get current timestamp for latest event
//            cal = Calendar.getInstance();
//            dateStr = dateFormat.format(cal.getTime());
//
////            String moisture = ms.readFromFile();
////            String light = ls.readFromFile();
////            String temperature = tp.readFromFile();
//
//            Levy lv = new Levy(dateStr, moisture, light, temperature);
//            LevyDAO.getInstance().addLevy(lv);
//
//            System.out.print("Light : " + light);
//            System.out.print("temperature : " + temperature);
//            System.out.print("moisture : " + moisture);
//            // System.out.print("***" + dateStr + ": " + (Integer.valueOf(val) * 60 / 5.5) / 60 + " l/min");
//            
//            
//            
//            if (Integer.valueOf(light) < 1000) {
//               // vanne.vanneOff();
////                red_pin.ledOn();
////                green_pin.ledOff();
//            } else {
//                //vanne.vanneOn();
////                green_pin.ledOn();
////                red_pin.ledOff();
//            }
//            // System.out.print("***" + dateStr + ": " + (Integer.valueOf(val) * 60 / 5.5) / 60 + " l/min");
//            try {
//                java.lang.Thread.sleep(sleepPeriod);
//                
//            } catch (InterruptedException ex) {
//                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
//            }
     //   }
    }

    protected static void stopAll() {

//        ms.endReading();
//        vanne.stopUse();
////        green_pin.stopUse();
////        red_pin.stopUse();
//        ls.endReading();
//        tp.endReading();
    }

}
