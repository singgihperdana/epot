/*
 I use this class to send json object ( last levy in db ) to android client thank's to socket

 */
package esprit.zendah.pfe;

import esprit.zendah.DAO.LevyDAO;
import esprit.zendah.entities.Levy;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Khalil
 */
public class HostThread extends Thread {

    private Socket hostThreadSocket;
    int cnt;

    HostThread(Socket socket, int c) {
        try {
            // hostThreadSocket = socket;
            hostThreadSocket = new Socket(socket.getInetAddress(), 9191);
        } catch (IOException ex) {
            Logger.getLogger(HostThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        cnt = c;
    }

    @Override
    public void run() {

        Levy lv = new Levy();
        try {
            lv = LevyDAO.getInstance().getLastLevyValues();
        } catch (SQLException ex) {
            Logger.getLogger(HostThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        JSONObject tab = new JSONObject();
        JSONObject obj = new JSONObject();
        try {
            obj.put("date", lv.getDate());
            obj.put("moisture", lv.getMoisture());
            obj.put("light", lv.getLight());
            obj.put("temperature", lv.getTemperature());

            tab.accumulate("last_levy", obj);
        } catch (JSONException ex) {
            Logger.getLogger(HostThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        OutputStream outputStream;
        try {
            outputStream = hostThreadSocket.getOutputStream();

            try (PrintStream printStream = new PrintStream(outputStream)) {
                printStream.print(tab.toString());
            }

        } catch (IOException ex) {
            Logger.getLogger(HostThread.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                hostThreadSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(HostThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
