/*
This class use the jdbc to have acces to db

*/

package esprit.zendah.db;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Connexion {

    static Connection conn;
    String url = "jdbc:sqlite:ipot.db";

    public static Connexion cnx;

    private Connexion() {

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection(url);
            System.out.println("connection etablie");

        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Connexion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static Connexion getInstance() {
        if (cnx == null) {
            cnx = new Connexion();

        } else {
            System.out.println("connection existante");
        }

        return cnx;

    }

    public static Connection getConn() {
        return conn;
    }

    public static void setConn(Connection conn) {
        Connexion.conn = conn;
    }

}
