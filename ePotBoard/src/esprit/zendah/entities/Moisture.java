/*
 this is the Moisture class entity, with all its methods and atributes
 */

package esprit.zendah.entities;

/**
 *
 * @author Khalil
 */
public class Moisture {

    private int id;
    private String max_wet;
    private String min_wet;
    private String right_wet;

    public Moisture() {
    }

    public Moisture(int id, String max_wet, String min_wet, String right_wet) {
        this.id = id;
        this.max_wet = max_wet;
        this.min_wet = min_wet;
        this.right_wet = right_wet;
    }
    
    

    public Moisture(String max_wet, String min_wet, String right_wet) {
        this.max_wet = max_wet;
        this.min_wet = min_wet;
        this.right_wet = right_wet;
    }

    @Override
    public String toString() {
        return "Moisture{" + "id=" + id + ", max_wet=" + max_wet + ", min_wet=" + min_wet + ", right_wet=" + right_wet + '}';
    }
    
    

    public int getId() {
        return id;
    }

    public String getMax_wet() {
        return max_wet;
    }

    public String getMin_wet() {
        return min_wet;
    }

    public String getRight_wet() {
        return right_wet;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMax_wet(String max_wet) {
        this.max_wet = max_wet;
    }

    public void setMin_wet(String min_wet) {
        this.min_wet = min_wet;
    }

    public void setRight_wet(String right_wet) {
        this.right_wet = right_wet;
    }
    
    
    
    

}
