/*
This class centralize all commun read-write registers operations
 */
package esprit.zendah.pin;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Khalil
 */
public class ConfigurationGPIO {

    String MyPin = "";
    static Boolean write = false;

//    }
    public void export() {

        writeToFile("/sys/class/gpio/export", MyPin);
        //System.out.println(MyPin);
    }

    public void unexport() {
        File exportFileCheck = new File("/sys/class/gpio/gpio" + MyPin);
        if (exportFileCheck.exists()) {
            writeToFile("/sys/class/gpio/unexport", MyPin);
           // System.out.println(MyPin);
        } else {
           // System.out.println("Port free");
        }

    }

    
    public void setDrive(String drive) {
        writeToFile("/sys/class/gpio/gpio" + MyPin + "/drive", drive);
    }

    public void setMode(String mode) {
        writeToFile("/sys/class/gpio/gpio" + MyPin + "/direction", mode);
    }

    public void setData(String data) {
        writeToFile("/sys/class/gpio/gpio" + MyPin + "/value", data);
    }

    private void writeToFile(String URI, String data) {
        try {
	            // delete the file

            // write the specified data to it
            try (FileWriter writer = new FileWriter(URI)) {
                writer.write(data);
                // close the file to apply the change
                writer.close();
               // System.out.println("Wrote " + "\"" + data + "\" to " + URI);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
