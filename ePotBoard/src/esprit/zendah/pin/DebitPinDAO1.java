/*
 Water flow sensor register access, pin A1, gpio 36
 */
package esprit.zendah.pin;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Khalil
 */
public class DebitPinDAO1 {

    ConfigurationGPIO cp;

    public void initReading() {

        cp = new ConfigurationGPIO();
//        cp.init();
        cp.MyPin="36";
        //cp.getPin(14);
        cp.unexport();
        cp.export();
        cp.setMode("out");
        cp.setData("0");

    }

    public String readFromFile() {
        String path = "/sys/devices/pci0000:00/0000:00:15.0/pxa2xx-spi.0/spi_master/spi0/spi0.0/iio:device0/in_voltage1_raw";
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
                return sCurrentLine;
            }

        } catch (IOException e) {
            System.out.println("++++exception : " + e);
        }

        return "";
    }

    public void endReading() {
        cp.unexport();
    }

}
