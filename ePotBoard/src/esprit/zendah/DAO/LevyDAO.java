/*
 This class is a Data Access Object alow  all levies operations, wich want to access to database
it implement the singelton model to have only one access instance and avoid problem DB Access
 */
package esprit.zendah.DAO;

import esprit.zendah.db.Connexion;
import esprit.zendah.entities.Levy;
import esprit.zendah.entities.Moisture;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
        
/**
 *
 * @author Khalil
 */
public class LevyDAO {

    private static Connection cnx;
    private static LevyDAO usrDAO;

    private LevyDAO() {
        cnx = Connexion.getInstance().getConn();
    }

    public static LevyDAO getInstance() {
        if (usrDAO == null) {
            usrDAO = new LevyDAO();
        }
        return usrDAO;
    }
    
    public Levy getLastLevyValues() throws SQLException {

        java.sql.PreparedStatement smt;
        smt = cnx.prepareStatement("select * from levies order by id desc limit 1");
        ResultSet rs = smt.executeQuery();
        rs.next();
        //System.out.println("rs" + rs.toString());
        System.out.println("rs" + rs.getInt("id"));
        Levy us = new Levy(rs.getString("date"), rs.getString("moisture"), rs.getString("light"), rs.getString("temperature"));
        smt.close();
        return us;

    }

    public void addLevy(Levy lv) throws SQLException {
        java.sql.PreparedStatement smt;
        smt = cnx.prepareStatement("insert into levies (date,moisture,light,temperature) values(?,?,?,?)");
        smt.setString(1, lv.getDate());
        smt.setString(2, lv.getMoisture());
        smt.setString(3, lv.getLight());
        smt.setString(4, lv.getTemperature());
        smt.executeUpdate();
        System.out.println("insertion effectuer avec succees");
        smt.close();
    }

    public void deleteLevy(int id) throws SQLException {
//		java.sql.PreparedStatement smt;
//
//		smt = cnx.prepareStatement("delete from users where us_id=?");
//		smt.setInt(1, id);
//		smt.executeUpdate();
//		System.out.println("suppression effectuer avec succees");
    }

//	public java.util.List<Levy> listerusers() throws SQLException {
//		java.sql.PreparedStatement smt;
//		java.util.List<User> l = new ArrayList<User>();
//
//		smt = cnx.prepareStatement("select * from users");
//		ResultSet rs = smt.executeQuery();
//
//		while (rs.next()) {
//			User f = new User(rs.getInt(1), rs.getInt(2), rs.getString(3),
//					rs.getString(4), rs.getString(5), rs.getString(6),
//					rs.getString(7));
//			l.add(f);
//		}
//
//		System.out.println("list terminer");
//		return l;
//	}
    public void updateLevy(Levy usr) throws SQLException {
//		java.sql.PreparedStatement smt;
//
//		smt = cnx
//				.prepareStatement("update users set us_ic_id=?,us_nom=?,us_prenom=?,us_mail=?,us_login=?,us_pwd where us_id=?");
//		smt.setInt(1, usr.getId_camera());
//		smt.setString(2, usr.getNom());
//		smt.setString(3, usr.getPrenom());
//		smt.setString(4, usr.getMail());
//		smt.setString(5, usr.getLogin());
//		smt.setString(6, usr.getPassword());
//		smt.setInt(7, usr.getId_user());
//
//		smt.executeUpdate();
//		System.out.println("modification effectuer avec succees");

    }

}
